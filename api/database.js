import { child, get, getDatabase, ref, set } from "firebase/database";
import { app } from "./app";

const database = getDatabase(app);


export function addBoard(userId, name) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}`)
        console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []

            console.log({ "boardName": name });
            data.push({ "boardName": name })

            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}

export function GetMyBoards(userId) {
    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            res(data)
        }).catch(err => rej(err.message))
    })
}

export function DropBoard(userId, boardId) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}`)
        console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            let newdata = []
            for (let i = 0; i <= data.length; i++) {
                if (i != boardId) {
                    if (data[i]!= undefined) newdata.push(data[i]);
                }
            }
            console.log(newdata);
            set(refUser, newdata);
            res(true)
        }).catch(err => rej(err.message))
    })
}

export function EditBoard(userId, name, boardId) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}`)
        console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            console.log(data);
            data["boardName"] = name;
            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}


export function AddColumn(userId, boardId, name) {
    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []

            console.log({ "columnName": name });
            data.push({ "columnName": name })

            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}

export function GetMyColumns(userId, boardId) {
    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            res(data)
        }).catch(err => rej(err.message))
    })
}


export function DropColumn(userId, boardId, columnId) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns`)
        console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            let newdata = []
            for (let i = 0; i <= data.length; i++) {
                if (i != columnId) {
                    if (data[i]!= undefined) newdata.push(data[i]);
                }
            }
            console.log(newdata);
            set(refUser, newdata);
            res(true)
        }).catch(err => rej(err.message))
    })
}

export function EditColumn(userId, colName, boardId, columnId) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns/${columnId}`)
        console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            console.log(data);
            data["columnName"] = colName;
            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}

export function AddTask(userId, boardId, columnId, name, description) {
    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks`)
        // console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            data.push({ "taskName": name, "taskDescription": description })

            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}

export function GetMyTasks(userId, boardId, columnId) {

    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            res(data)
        }).catch(err => rej(err.message))
    })
}

export function DropTask(userId, boardId, columnId, taskId) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks`)
        console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            let newdata = []
            for (let i = 0; i <= data.length; i++) {
                if (i != taskId) {
                    if (data[i]!= undefined) newdata.push(data[i]);
                }
            }
            console.log(newdata);
            set(refUser, newdata);
            res(true)
        }).catch(err => rej(err.message))
    })
}

export function EditTask(userId, taskName, description, boardId, columnId, taskId) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks/${taskId}`)
        console.log(refUser);
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            console.log(data);
            data["taskName"] = taskName;
            data["taskDescription"] = description;
            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}