// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAktimZjKvUeJ8MsaMkvIqmQIEmVZTvjxY",
  authDomain: "livecampus-projet-react-native.firebaseapp.com",
  projectId: "livecampus-projet-react-native",
  storageBucket: "livecampus-projet-react-native.appspot.com",
  messagingSenderId: "185149305012",
  appId: "1:185149305012:web:79a92446e79b83f013e7e1",
  measurementId: "G-TZE5EKWH3S",
  databaseURL: "https://livecampus-projet-react-native-default-rtdb.europe-west1.firebasedatabase.app/"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);