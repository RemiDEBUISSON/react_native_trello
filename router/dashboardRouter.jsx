import { createStackNavigator } from '@react-navigation/stack';
import { ListeBoardsVue } from '../vues/board/listeBoardsVue';
import { NewBoard } from '../vues/board/newBoard';
import { EditBoardVue } from '../vues/board/editBoardVue';
import { EditColVue } from '../vues/col/editColVue';
import { EditTaskVue } from '../vues/task/editTaskVue';
import { ListColView } from '../vues/col/listCol';
import { NewCol } from '../vues/col/newCol';
import { ListTaskView } from '../vues/task/listTask';
import { NewTask } from '../vues/task/newTask';

const Stack = createStackNavigator();

export function DashboardRouter() {
    return (
        <Stack.Navigator tabBarPosition="bottom">
            {/* <Stack.Group screenOptions={{ headerShown: false }}>
                <Stack.Screen name="colonnes" component={ColRouter} />
            </Stack.Group> */}
            
            <Stack.Screen name="Dashboard" component={ListeBoardsVue} />
            
            <Stack.Group>
                <Stack.Screen name="Créer un tableau" component={NewBoard} />
            </Stack.Group>

            <Stack.Group>
                <Stack.Screen name="Editer un tableau" component={EditBoardVue} />
            </Stack.Group>


            <Stack.Group>
                <Stack.Screen name="colonnes" component={ListColView} />
            </Stack.Group>

            <Stack.Group >
                <Stack.Screen name="nouvelle colonne" component={NewCol} />
            </Stack.Group>

            <Stack.Group>
                <Stack.Screen name="Editer une colonne" component={EditColVue} />
            </Stack.Group>


            <Stack.Group >
                <Stack.Screen name="taches" component={ListTaskView} />
            </Stack.Group>

            <Stack.Group >
                <Stack.Screen name="nouvelle tache" component={NewTask} />
            </Stack.Group>

            <Stack.Group>
                <Stack.Screen name="Editer une tache" component={EditTaskVue} />
            </Stack.Group>
        </Stack.Navigator>
    )
}
