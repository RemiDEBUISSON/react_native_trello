import { createStackNavigator } from '@react-navigation/stack';
import { ListColView } from '../vues/col/listCol';
import { NewCol } from '../vues/col/newCol';

const Stack = createStackNavigator();

export function ColRouter() {
    return (
        <Stack.Navigator tabBarPosition="bottom">

            <Stack.Screen name="colonnes" component={ListColView} />

            <Stack.Group screenOptions={{ presentation: 'modal' }} >
                <Stack.Screen name="Créer une colonne" component={NewCol} />
            </Stack.Group>

        </Stack.Navigator>
    )
}