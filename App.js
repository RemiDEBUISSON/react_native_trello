import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { UserRouter } from './router/userRouter';
import { DashboardContext } from './context'
import { DashboardRouter } from './router/dashboardRouter';

export default function App() {
  const [user, setuser] = useState(null);
  const [boards, setboards] = useState([]);
  const [cols, setcols] = useState([]);
  const [tasks, settasks] = useState([]);
  return (
    <DashboardContext.Provider value={{ user, setuser, boards, setboards, cols, setcols, tasks, settasks }} >
      <NavigationContainer>
        {(user) ? <DashboardRouter></DashboardRouter> : <UserRouter></UserRouter>}

      </NavigationContainer>
    </DashboardContext.Provider>
  );
}

