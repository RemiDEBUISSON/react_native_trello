import { useContext, useEffect } from "react";
import { ScrollView, View } from "react-native";
import { GetMyBoards } from "../../api/database";
import { Board } from "../../components/board";
import { DashboardContext } from "../../context";
import { styles } from "../../styles";
import { Link, useNavigation } from '@react-navigation/native';

export function ListeBoardsVue() {

    const { user, boards, setboards } = useContext(DashboardContext);
    useEffect(() => {

        GetMyBoards(user.uid).then(data => {
            setboards(data);
        })
    }, []);
    const navigation = useNavigation();

    return (
        <ScrollView style={styles.boards}>

            <Link style={styles.btnAddBoard} to={{ screen: 'Créer un tableau' }}>
                add board
            </Link>

            <View style={styles.boards}>
                {boards.map((board, index) => {
                    return <Board key={index} nom={board.boardName} dashboardId={index} navigation={navigation} user={user} />
                })}
            </View>
        </ScrollView>
    )
}