import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView, Text } from 'react-native'
import { EditBoard, DropBoard } from '../../api/database';
import { MyBout } from '../../components/bout';
import { MyInput } from '../../components/input'
import { DashboardContext } from "../../context";
import { styles } from '../../styles';

export function EditBoardVue({ route }) {
    const{dashboardId}=route.params
    // const{boardName}=route.params
    const[board, setboard]=useState(route.params.boardName);

    // const [board, setboard] = useState(route.params.name);
    const { user, boards, setboards } = useContext(DashboardContext);

    const modifyBoard = () => {
        Keyboard.dismiss()
        EditBoard(user.uid, board, dashboardId).then(() => {
            Alert.alert('Tableau modifié')
            let newboards = [...boards];
            newboards[dashboardId] = board;
            setboards(newboards)
        }).catch(err => Alert.alert(err))
    }

    const suppBoard = () => {
        Keyboard.dismiss()
        DropBoard(user.uid, dashboardId).then(() => {
            Alert.alert('Tableau modifié')
            let newboards = []
            for (let i = 0; i < boards.length; i++) {
                if (i != dashboardId) {
                    if (boards[i]!= undefined) newboards.push(boards[i]);
                }
            }
            setboards(newboards)
        }).catch(err => Alert.alert(err))
    }

    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <Text>Tableau : {dashboardId}</Text>
                <MyInput label={"board"} valeur={board} etat={setboard} />
                <MyBout label="Editer" click={modifyBoard} />
                <MyBout label="Supprimer" click={suppBoard} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}