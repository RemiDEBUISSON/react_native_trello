import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView } from 'react-native'
import { addBoard } from '../../api/database';
import { MyBout } from '../../components/bout';
import { MyInput } from '../../components/input'
import { DashboardContext } from '../../context';
import { styles } from '../../styles';

export function NewBoard() {
    const [board, setboard] = useState("");
    const { user, setboards, boards } = useContext(DashboardContext);
    const handleClick = () => {
        Keyboard.dismiss()
        addBoard(user.uid, board).then(() => {
            setboards([...boards, board])
        }).catch(err => Alert.alert(err))
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <MyInput label={"Ajouter une nouvelle colonne"} valeur={board} etat={setboard} />
                <MyBout label="Valider" click={handleClick} />
            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>
    )
}