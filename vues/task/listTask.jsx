import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { styles } from '../../styles';
import { useContext, useEffect } from "react";
import { DashboardContext } from "../../context";
import { GetMyTasks } from "../../api/database";
import {Task} from "../../components/task"

export function ListTaskView({ route, navigation }) {

    const { user, tasks, settasks } = useContext(DashboardContext);
    const{ dashboardId, colId } = route.params;

    useEffect(() => {
        
        GetMyTasks(user.uid, dashboardId, colId).then(data => {
            settasks(data);
        })
    }, []);

    return (
        <ScrollView style={styles.boards}>

            <TouchableOpacity style={[styles.btnAddBoard]} onPress={ () => navigation.navigate('nouvelle tache',{dashboardId: dashboardId, colId: colId}) }>
                <Text>
                    add task
                </Text>
            </TouchableOpacity>
            <View style={styles.boards}>
                {tasks.map((element, key) => {
                    return (
                        <Task key={key} nom={element.taskName} description={element.taskDescription} dashboardId={dashboardId} colId={colId} navigation={navigation} user={user} taskId={key} />
                    )
                })}
            </View>

            {/* <View style={styles.boards}>
                {cols.map((col, index) => {
                    return <Column key={index} nom={col.columnName} dashboardId={index} navigation={navigation} />
                })}
            </View> */}
        </ScrollView>
    )
}