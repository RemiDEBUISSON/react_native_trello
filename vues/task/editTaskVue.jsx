import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView, Text } from 'react-native'
import { EditTask, DropTask } from '../../api/database';
import { MyBout } from '../../components/bout';
import { MyInput } from '../../components/input'
import { DashboardContext } from "../../context";
import { styles } from '../../styles';

export function EditTaskVue({ route }) {
    const{dashboardId}=route.params
    const{columnId}=route.params
    const{taskId}=route.params
    const{taskName}=route.params
    const{taskDescription}=route.params
    const[task, settask]=useState(route.params.taskName);
    const[desc, setdesc]=useState(route.params.taskDescription);

    const { user, tasks, settasks } = useContext(DashboardContext);

    const modifyTask = () => {
        Keyboard.dismiss()
        EditTask(user.uid, task, desc, dashboardId, columnId, taskId).then(() => {
            Alert.alert('Colonne modifié')
            let newtasks = [...tasks];
            newtasks[taskId] = task;
            settasks(newtasks)
        }).catch(err => Alert.alert(err))
    }

    const suppTask = () => {
        Keyboard.dismiss()
        DropTask(user.uid, dashboardId, columnId, taskId).then(() => {
            Alert.alert('task supprimé')
        }).catch(err => Alert.alert(err))
    }

    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <Text>Tableau : {taskName}</Text>
                <Text>Description : {taskDescription}</Text>
                
                <MyInput label={"colonne"} valeur={task} etat={settask} />
                <MyInput label={"description"} valeur={desc} etat={setdesc} />
                <MyBout label="Editer" click={modifyTask} />
                <MyBout label="Supprimer" click={suppTask} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}