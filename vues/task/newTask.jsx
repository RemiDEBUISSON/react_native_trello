import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Keyboard, SafeAreaView } from 'react-native'
import { MyBout } from '../../components/bout';
import { AddTask } from '../../api/database';
import { MyInput } from '../../components/input'
import { DashboardContext } from '../../context';
import { styles } from '../../styles';

export function NewTask({ route }) {
    const{dashboardId}=route.params
    const{colId}=route.params

    const [task, settask] = useState("");
    const [description, setdescription] = useState("");

    const { user, tasks, settasks } = useContext(DashboardContext);
    const handleClick = () => {
        Keyboard.dismiss()
        AddTask(user.uid, dashboardId, colId, task, description).then(() => {
            settasks([...tasks, task])
        }).catch(err => Alert.alert(err))
    }

    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <MyInput label={"Ajouter une nouvelle tache"} valeur={task} etat={settask} />
                <MyInput label={"description"} valeur={description} etat={setdescription} />
                <MyBout label="Valider" click={handleClick} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}