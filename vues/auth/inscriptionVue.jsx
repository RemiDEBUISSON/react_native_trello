import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, SafeAreaView } from 'react-native'
import { createUser } from '../../api/auth';
import { MyBout } from '../../components/bout';
import { MyInput } from '../../components/input'
import { DashboardContext } from '../../context';
import { styles } from '../../styles';

export function InscriptionVue() {
    const [login, setlogin] = useState("");
    const [mdp, setmdp] = useState("");
    const [mdp2, setmdp2] = useState("");
    const { setuser } = useContext(DashboardContext);
    const handleClick = () => {
        Keyboard.dismiss()
        if (mdp === mdp2) {
            createUser(login, mdp).then(data => {
                setuser(data);
            }).catch(err => Alert.alert(err))
        } else {
            Alert.alert("Les mots de passe sont differents")
        }
    }
    return (
        <SafeAreaView style={styles.safe}>


            <View style={styles.contConnect}>
                <MyInput label={"login"} valeur={login} etat={setlogin} />
                <MyInput password={true} label={"Mot de passe"} valeur={mdp} etat={setmdp} />
                <MyInput password={true} label={"Confirmez"} valeur={mdp2} etat={setmdp2} />
                <MyBout label="Valider" click={handleClick} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}