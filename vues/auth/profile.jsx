import { StatusBar } from 'expo-status-bar';
import { useContext } from 'react';
import { View, Keyboard, SafeAreaView } from 'react-native'
import { MyBout } from '../../components/bout';
import { DashboardContext } from '../../context';
import { styles } from '../../styles';

export function Profile() {
    const { setuser } = useContext(DashboardContext);
    const logout = () => {
        Keyboard.dismiss()
            setuser("");
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                
                <MyBout label="déconnexion" click={logout} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}