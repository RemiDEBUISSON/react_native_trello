import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { styles } from '../../styles';
import { useContext, useEffect } from "react";
import { DashboardContext } from "../../context";
import { GetMyColumns } from "../../api/database";
import { Column } from "../../components/column";

export function ListColView({ route, navigation }) {

    const { user, cols, setcols } = useContext(DashboardContext);
    const{ dashboardId } = route.params;

    useEffect(() => {
        
        GetMyColumns(user.uid, dashboardId).then(data => {
            setcols(data);
        })
    }, []);

    return (
        <ScrollView style={styles.boards}>

            <TouchableOpacity style={[styles.btnAddBoard]} onPress={ () => navigation.navigate('nouvelle colonne',{dashboardId: dashboardId}) }>
                <Text>
                    add column
                </Text>
            </TouchableOpacity>

            <View style={styles.boards}>
                {cols.map((col, index) => {
                    return <Column key={index} nom={col.columnName} dashboardId={dashboardId} colId={index} navigation={navigation} user={user} />
                })}
            </View>
        </ScrollView>
    )
}