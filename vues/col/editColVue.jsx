import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView, Text } from 'react-native'
import { EditColumn, DropColumn } from '../../api/database';
import { MyBout } from '../../components/bout';
import { MyInput } from '../../components/input'
import { DashboardContext } from "../../context";
import { styles } from '../../styles';

export function EditColVue({ route }) {
    const{dashboardId}=route.params
    const{ColumnId}=route.params
    const{columnName}=route.params
    // const{boardName}=route.params
    const[col, setcol]=useState(route.params.colName);

    // const [board, setboard] = useState(route.params.name);
    const { user, cols, setcols } = useContext(DashboardContext);

    const modifyColumn = () => {
        Keyboard.dismiss()
        // userId, colName, boardId, columnId
        EditColumn(user.uid, col, dashboardId, ColumnId).then(() => {
            Alert.alert('Colonne modifié')
            let newcols = [...cols];
            newcols[ColumnId] = col;
            setcols(newcols)
        }).catch(err => Alert.alert(err))
    }

    // supp column
    const suppColumn = () => {
        Keyboard.dismiss()
        DropColumn(user.uid, dashboardId, ColumnId).then(() => {
            Alert.alert('Colonne supprimé')
        }).catch(err => Alert.alert(err))
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <Text>Tableau : {columnName}</Text>
                <MyInput label={"colonne"} valeur={col} etat={setcol} />
                <MyBout label="Editer" click={modifyColumn} />
                <MyBout label="Supprimer" click={suppColumn} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}