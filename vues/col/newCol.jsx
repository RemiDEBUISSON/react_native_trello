import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Keyboard, SafeAreaView } from 'react-native'
import { MyBout } from '../../components/bout';
import { AddColumn } from '../../api/database';
import { MyInput } from '../../components/input'
import { DashboardContext } from '../../context';
import { styles } from '../../styles';

export function NewCol({ route }) {
    const{dashboardId}=route.params

    const [col, setcol] = useState("");
    const { user, cols, setCols } = useContext(DashboardContext);
    const handleClick = () => {
        Keyboard.dismiss()
        AddColumn(user.uid, dashboardId, col).then(() => {
            // Alert.alert(boards)
            setCols([...cols, col])
        }).catch(err => Alert.alert(err))
    }

    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <MyInput label={"Ajouter une nouvelle colonne"} valeur={col} etat={setcol} />
                <MyBout label="Valider" click={handleClick} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}