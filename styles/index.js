import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    contener: {
        flex: 1,
        alignItems: "center"
    },
    safe: {
        marginTop: 40,
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    input: {
        height: 50,
        borderColor: "#000",
        borderWidth: 1,
        width: 300
    },
    labelInput: {
        fontSize: 20,
        fontWeight: "bold"
    },
    contConnect: {
        height: 300
    },
    btnAddBoard: {
        textAlign: "center",
        padding: 15,
        margin: 20,
        marginLeft: 40,
        width: 300,
        height: 50,
        backgroundColor: "#FF7E7E",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10
    },
    btn: {
        height: 20,
        width: 300,
        backgroundColor: "#FF7E7E",
        borderRadius: 100,
        flex: 0.5,
        alignItems: "center",
        justifyContent: "center"
    },
    tache: {
        width: 300,
        backgroundColor: "#FF7E7E",
        margin: 10,
        paddingVertical: 5
    },

    tacheTxt: {
        textAlign: "center"
    },

    boards: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        width: 500
    },

    boardName: {
        width: 170,
        height: 100,
        margin: 10,
        justifyContent: "center",
        backgroundColor: "#FF7E7E",
        borderRadius: 10,
        padding: 10,
        textAlign: "center",
        verticalAlign: "middle",
        lineHeight: 80,
        color: "#F0F0F0"
    },
    supp: {
        position: "absolute",
        right: 0,
        top: 0,
        backgroundColor: "#FF7E7E",
        width: 20,
        height: 20,
        borderRadius: 100,
        textAlign: "center",
        lineHeight: 20,
        color: "#F0F0F0"
    },
    edit: {
        position: "absolute",
        right: 0,
        top: 20,
        backgroundColor: "#FF7E7E",
        width: 20,
        height: 20,
        borderRadius: 100,
        textAlign: "center",
        lineHeight: 20,
        color: "#F0F0F0"
    },
    

})