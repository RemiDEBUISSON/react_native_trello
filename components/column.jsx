import { TouchableOpacity, Text } from 'react-native'
import { styles } from '../styles'
import { Keyboard } from 'react-native'
import { DropColumn } from '../api/database' 


export function Column({ nom, dashboardId, colId, navigation, user }) {
    // supp column
    const suppColumn = () => {
        Keyboard.dismiss()
        DropColumn(user.uid, dashboardId, colId).then(() => {
            Alert.alert('Colonne modifié')
        }).catch(err => Alert.alert(err))
    }

    const editColumn = () => {
        navigation.navigate('Editer une colonne', { dashboardId: dashboardId, ColumnId:colId, columnName:nom })
    }

    return (
        <TouchableOpacity style={[styles.boardName]} onPress={ () => navigation.navigate('taches',{dashboardId: dashboardId, colId:colId}) } > 
               
            <Text style={styles.labelInput}>{nom}</Text>

            {/* drop column */}
            <TouchableOpacity style={[styles.supp]} onPress={suppColumn} >
                <Text style={styles.labelInput}>X</Text>
            </TouchableOpacity>

            {/* edit column */}
            <TouchableOpacity style={[styles.edit]} onPress={editColumn} >
                <Text style={styles.labelInput}>Edit</Text>
            </TouchableOpacity>

        </TouchableOpacity>

    )
}