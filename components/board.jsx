import { TouchableOpacity, Text } from 'react-native'
import { styles } from '../styles'
import { DropBoard } from '../api/database' 
import { Keyboard } from 'react-native'

export function Board({ nom, dashboardId, navigation, user }) {


    const suppBoard = () => {
        Keyboard.dismiss()
        DropBoard(user.uid, dashboardId).then(() => {
            Alert.alert('Tableau modifié')
            let newboards = []
            for (let i = 0; i < boards.length; i++) {
                if (i != dashboardId) {
                    if (boards[i]!= undefined) newboards.push(boards[i]);
                }
            }
            setboards(newboards)
        }).catch(err => Alert.alert(err))
    }

    const editBoard = () => {
        navigation.navigate('Editer un tableau', { dashboardId: dashboardId, boardName:nom })
    }

    return (
        <TouchableOpacity style={[styles.boardName]} onPress={ () => navigation.navigate('colonnes',{ dashboardId: dashboardId }) } >
        
            <Text style={styles.labelInput}>{nom}</Text>

            <TouchableOpacity style={[styles.supp]} onPress={suppBoard} >
                <Text style={styles.labelInput}>X</Text>
            </TouchableOpacity>
            {/* edit */}
            <TouchableOpacity style={[styles.edit]} onPress={editBoard} >
                <Text style={styles.labelInput}>Edit</Text>
            </TouchableOpacity>

        </TouchableOpacity>
    )
}