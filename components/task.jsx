import { TouchableOpacity, Text } from 'react-native'
import { styles } from '../styles'
import { Keyboard } from 'react-native'
import { DropTask } from '../api/database' 


export function Task({ nom, description, dashboardId, colId, taskId, navigation, user }) {
    // supp column
    const suppTask = () => {
        Keyboard.dismiss()
        DropTask(user.uid, dashboardId, colId, taskId).then(() => {
            Alert.alert('task supprimé')
        }).catch(err => Alert.alert(err))
    }

    const editTask = () => {
        navigation.navigate('Editer une tache', { dashboardId: dashboardId, columnId:colId, taskName:nom, taskDescription:description, taskId:taskId })
    }

    return (
        <TouchableOpacity style={styles.boardName} > 
    
            <Text style={styles.labelInput}>{nom}</Text>
            <Text style={styles.labelInput}>{description}</Text>
            
            <TouchableOpacity style={[styles.edit]} onPress={editTask} >
                <Text style={styles.labelInput}>Edit</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.supp]} onPress={suppTask} >
                <Text style={styles.labelInput}>X</Text>
            </TouchableOpacity>
    
        </TouchableOpacity>
    )
}